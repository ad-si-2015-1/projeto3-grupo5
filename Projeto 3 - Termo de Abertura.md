Termo de Abertura do Projeto
Projeto 3 - Jogo da Memória em RMI
Versão
1.0

##### Objetivos deste documento
Construir um jogo da memória multiusuário, utilizando tecnologia RMI.

##### Justificativa do projeto
Requisito parcial para andamento da disciplina de Aplicações Distribuídas.

##### Situação atual
O projeto encontra-se na fase de elaboração da documentação, e concomitantemente, a implementação do próprio software.

##### Produtos e principais requisitos
- Rodar em ambientes Windows e Linux;
- Utilizar tecnologia RMI;
- Utilizar threads;
- Utilizar para controle de versão o ambiente GitLab;
- Jogo multiusuário;
- Vence quem encontrar o maior número de peças iguais;
- Se o jogador acertar, ele joga novamente, se errar passa a vez.

##### Marcos
- Elaboração do Termo de Abertura do Projeto: 03/05/2015
- Elaboração de documentações auxiliares: 03/05/2015
- Encerramento do Projeto: 12/05/2015

##### Partes interessadas do Projeto

##### - Equipe:
- Allan Braga (Documentador);
- Farid Chaud (Documentador);
- Mauro Henrique (Desenvolvedor);
- Miriã Laís (Líder de equipe e Documentadora).

##### -  Auxílio e avaliação:
Marcelo Akira.

##### Restrições
A principal limitação do projeto refere-se ao tempo destinado à elaboração e implementação do mesmo.

##### Premissas
O fator essencial para o sucesso do andamento do projeto, refere-se ao alinhamento e dedicação da equipe responsável pelo mesmo.

##### Riscos
- Escassez do tempo;
- Disponibilidade da equipe;
- Mudanças drásticas reconhecidas no decorrer do projeto;
- Priorizações incorretas;
- Fase má elaborada.

##### Aprovações
Termo de Abertura aprovado em: 03/05/2015.