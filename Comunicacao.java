package memoria;

import java.rmi.*;

public interface Comunicacao extends Remote { //interface RMI
    
    public void tratar(String texto) throws RemoteException;
    
    public int getMostrar() throws RemoteException;
    public void setMostrar() throws RemoteException;
    
    public Tabuleiro getTabuleiro() throws RemoteException;
    
    public int getClick() throws RemoteException;
    
    public boolean getVez() throws RemoteException;
    public void setVez() throws RemoteException;
}
